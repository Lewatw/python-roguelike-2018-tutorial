class Stats:
    def __init__(self, turns_taken=0):
        self.turns_taken = turns_taken


    def increase_turn_count_by(self, number_of_turns):
        self.turns_taken += number_of_turns

    @property
    def get_number_of_turns_taken(self):
        return self.turns_taken


