import unittest

from loader_functions.initialise_new_game import get_constants, get_game_variables
from game_states import GameStates


class TestLoadConstants(unittest.TestCase):

    """ check window has a title """

    def test_window_has_title(self):
        cons = get_constants()
        self.assertNotEqual(cons['window_title'], None)

    def test_basic_game_setup(self):
        cons = get_constants()
        player, entities, game_map, message_log, game_state = get_game_variables(cons)

        self.assertNotEqual(player, None)
        self.assertNotEqual(entities, None)
        self.assertNotEqual(game_map, None)
        self.assertNotEqual(message_log, None)
        self.assertEqual(game_state, GameStates.PLAYERS_TURN)


if __name__ == '__main__':
    unittest.main()
